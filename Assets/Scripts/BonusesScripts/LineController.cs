﻿using UnityEngine;

public class LineController : MonoBehaviour {

    public GameObject line;

	// Use this for initialization
	void Start () {
        
        Deactivate();
	}
    public void Activate()
    {
        line.GetComponent<BoxCollider2D>().enabled = true;
        line.GetComponent<SpriteRenderer>().enabled = true;
    }
    public void Deactivate()
    {
        line.GetComponent<BoxCollider2D>().enabled = false;
        line.GetComponent<SpriteRenderer>().enabled = false;
    }
}
