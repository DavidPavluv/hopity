﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class Menu : MonoBehaviour
{
    public Text text;
    public Text highScore;

    private void Start()
    {
        
        if(PlayerPrefs.GetInt("HighScore",0) == 0)
        {
            text.text = "";
            highScore.text = "";
        } else
        {
            text.text = "HIGHSCORE";
            highScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        }

        Advertisement.Initialize("2799508");
    }

    public void LoadGame()
    {
        SceneManager.LoadScene(1);
    }
    public void QuitGame()
    {
        Debug.Log("Quitting...");
        Application.Quit();
    }

    public void LoadShop()
    {
        SceneManager.LoadScene(2);
    }
}
