﻿using UnityEngine;
using UnityEngine.UI;

public class MoneyManager : MonoBehaviour {

    public GameObject moneyIcon;
    public Text textAmount;
    private int moneyAmount;


	void Start () {
            moneyAmount = PlayerPrefs.GetInt("money");
            moneyIcon.SetActive(true);
            textAmount.text = moneyAmount.ToString();
        
	}
	
    public void addMoney(int amount) {
        moneyIcon.SetActive(true);
        moneyAmount += amount;
        PlayerPrefs.SetInt("money", moneyAmount);

        textAmount.text = moneyAmount.ToString();
    }

    public void deleteMoney(int amount)
    {

        moneyAmount -= amount;

        if(moneyAmount == 0)
        {
            moneyIcon.SetActive(false);
        }
        else
            moneyIcon.SetActive(true);

        PlayerPrefs.SetInt("money", moneyAmount);
        textAmount.text = moneyAmount.ToString();
    }
}
