﻿using UnityEngine;

public class Splash : MonoBehaviour {

    public AudioClip splashClip;

    void Start () {
        
        GetComponent<AudioSource>().clip = splashClip;
        GetComponent<AudioSource>().Play();
    }	
}
