﻿using UnityEngine;

public class SoundTrack : MonoBehaviour {

    public static bool isPlaying = false;

	void Start () {
        if(isPlaying == false)
        {
            isPlaying = true;
            GetComponent<AudioSource>().Play();
            DontDestroyOnLoad(GetComponent<AudioSource>());
        }
    }
}
