﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;
    public GameObject GameOverPanel;

    //highscore
    public Text best;
    public Text bestScore;

    private void Start()
    {
        SetBest();
    }

    public void SetBest()
    {
    if (PlayerPrefs.GetInt("HighScore", 0) == 0)
            {
                best.text = "";
                bestScore.text = "";
            }
            else if (PlayerPrefs.GetInt("HighScore", 0) > 0)
            {
                best.text = "HIGHSCORE";
                bestScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        }
    }
    public void PauseClick()
    {
        if (GameIsPaused)
        {
            Resume();
        } else
        {
            Pause();
        }
    }

    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
    public void LoadShop()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(2);
    }

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;

        
    }

    public void LoadMenu()
    {
        
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }
    public void QuitGame()
    {
        Debug.Log("Quitting...");
        Application.Quit();
    }
 

    public void RestartGame()
    {
 
        GameOverPanel.SetActive(false);
        Time.timeScale = 1f;
        SceneManager.LoadScene("Game");        
        ShowVideoAd();
    }
    
    public void ShowVideoAd()
    {
        if (Advertisement.IsReady("video"))
        {
            GameObject.Find("AudioManager").GetComponent<AudioSource>().Stop();
            Time.timeScale = 0f;
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("video", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown. Thanks!");
                Time.timeScale = 1f;
                GameObject.Find("AudioManager").GetComponent<AudioSource>().Play();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end. Eh..");
                Time.timeScale = 1f;
                GameObject.Find("AudioManager").GetComponent<AudioSource>().Play();
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                Time.timeScale = 1f;
                GameObject.Find("AudioManager").GetComponent<AudioSource>().Play();
                break;
        }
    }

}
