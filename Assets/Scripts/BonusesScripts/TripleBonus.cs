﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TripleBonus : MonoBehaviour {

    public AudioClip tick;

    public Text timer;

    private void Start()
    {
        GetComponent<AudioSource>().clip = tick;
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            FindObjectOfType<GameManager>().timeTriple += 10; 
            GetComponent<AudioSource>().Play();
            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            Destroy(gameObject, 12f);
        }
    }

}
