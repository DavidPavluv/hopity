﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Shop : MonoBehaviour {

    public GameObject floatingText;
    public Text moneyAmount;
    public List<GameObject> Children;

    private void Start()
    {
        moneyAmount.text = PlayerPrefs.GetInt("money", 0).ToString();
    }


    //Buy
    public void BuyClick(GameObject o)
    {


        if (PlayerPrefs.GetInt("money", 0) >= 100)
        {
            GameObject.FindObjectOfType<MakeAnim>().MakeAnimation();
            PlayerPrefs.SetInt("money", PlayerPrefs.GetInt("money", 0) - 100);
            moneyAmount.text = PlayerPrefs.GetInt("money", 0).ToString();
            o.transform.GetChild(1).gameObject.SetActive(true);

            string i = o.transform.name;
            PlayerPrefs.SetInt("bought" + i, 1);
        }
        else
            MakeMoneyInfo(o);
        
    }
    public void MakeMoneyInfo(GameObject o)
    {
        Instantiate(floatingText, o.transform.position, Quaternion.Euler(new Vector3(0, 0, Random.Range(-30,30))));
    }
    
    //use
    public void UseClick(GameObject o)
    {     
        o.transform.Find(o.transform.name).gameObject.SetActive(true);
        string i = o.gameObject.transform.name;
        PlayerPrefs.SetString("Selected", i);

    }

    public void LoadGame()
    {
        SceneManager.LoadScene(1);
    }

    public void UnCheckedAll() {
        GameObject[] used = GameObject.FindGameObjectsWithTag("USED");

        foreach(GameObject image in used)
        {
            image.SetActive(false);
        }
    }
}

