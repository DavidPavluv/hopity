﻿using UnityEngine;

public class BulletScreen : MonoBehaviour {
    public float velY = 10;
    float velX = 0;
    public float destroyTime = 1f;
    Rigidbody2D rb;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}

    void Update()
    {
        rb.velocity = new Vector2(velX, velY);
        Destroy(gameObject, destroyTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        DestroyMe();
    }

    private void DestroyMe()
    {
        Destroy(gameObject);
    }
}
