﻿using UnityEngine;

public class MakeAnim : MonoBehaviour {

    Animator _animator;


	// Use this for initialization
	void Start () {
        _animator = GetComponent<Animator>();
	}

    public void MakeAnimation()
    {
        _animator.Play("addScore");
    }
}
