﻿using System.Collections;
using UnityEngine;

public class LineBonus : MonoBehaviour {
    public AudioClip tick;

    private void Start()
    {
        GetComponent<AudioSource>().clip = tick;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            FindObjectOfType<GameManager>().timeLine += 5;
            GetComponent<AudioSource>().Play();
            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            Destroy(gameObject, 100f);
        }    
    }


}
