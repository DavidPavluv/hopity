﻿using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    public int hp = 60;
    public Text hptext;
    public ParticleSystem explode;
    public AudioClip coin;
    public GameObject floatCoin;

    private void Start()
    {

        GetComponent<AudioSource>().clip = coin;
        int count = GameObject.FindGameObjectsWithTag("enemy").Length;       
        GetComponent<SpriteRenderer>().sortingOrder = FindObjectOfType<GameManager>().counter;
        GetComponentInChildren<Canvas>().sortingOrder = FindObjectOfType<GameManager>().counter;
        hptext.text = hp.ToString();        
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("bullet"))
        {
            GetComponent<AudioSource>().Play();
            hp--;
            FindObjectOfType<GameManager>().AddScore(1);
            hptext.text = hp.ToString();
        
            if(hp == 0)
            {
                DropCoin();
                Killed();
                
            }
        }

        if (col.gameObject.tag.Equals("killer")){

            DropCoin();
            KilledByKiller();           
        }
        
    }

    private void Killed()
    {
                GameObject.FindObjectOfType<MakeAnim>().MakeAnimation();
                GameObject.FindObjectOfType<MoneyManager>().addMoney(1);
                GetComponent<AudioSource>().Play();
                DestroyEnemy(); 
    }

    private void KilledByKiller()
    {
            GameObject.FindObjectOfType<MakeAnim>().MakeAnimation();
            GameObject.FindObjectOfType<MoneyManager>().addMoney(1);
            FindObjectOfType<GameManager>().AddScore(hp);
            GetComponent<AudioSource>().Play();
            DestroyEnemy();
    }

    private void DropCoin()
    {
        Instantiate(floatCoin, transform.position, Quaternion.identity);

    }

    private void DestroyEnemy()
    {
        Instantiate(explode, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }      

}
