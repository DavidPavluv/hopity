﻿using UnityEngine;

public class UsedOrNot : MonoBehaviour {
    private string i;

    public void Start()
    {
        i = gameObject.transform.name;
        MakeMe();
        
    }

    private void MakeMe()
    {
        if (PlayerPrefs.GetString("Selected", "1").Equals(i))
                {
                    gameObject.SetActive(true);
                }
                else
                    gameObject.SetActive(false);
    }
}
