﻿using UnityEngine;

public class FloatingCoin : MonoBehaviour {

    public float DestroyTime = 1f;
    void Start()
    {
        Destroy(gameObject, DestroyTime);
    }
}
