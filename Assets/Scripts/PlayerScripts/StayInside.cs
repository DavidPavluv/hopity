﻿using UnityEngine;

public class StayInside : MonoBehaviour {

    private float leftLimitation, rightLimitation;

    private void Start()
    {
        leftLimitation = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)).x + transform.lossyScale.x / 2 - 0.5f;
        rightLimitation = Camera.main.ViewportToWorldPoint(new Vector2(1, 0)).x - transform.lossyScale.x / 2 + 0.5f;
    }
    void Update () {
        Physics2D.GetIgnoreLayerCollision(8, 9);
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, leftLimitation, rightLimitation), Mathf.Clamp(transform.position.y, transform.position.y, 4f));
	}
}
