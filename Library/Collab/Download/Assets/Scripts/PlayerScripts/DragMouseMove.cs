﻿using UnityEngine;
using UnityEngine.UI;

public class DragMouseMove : MonoBehaviour {

    private Vector3 mousePosition;
    private Rigidbody2D rb;
    private Vector2 direction;
    private float moveSpeed = 300f;
    public AudioClip gameOverClip;
    public Text scoreText;

    void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	void FixedUpdate () {
        if (Input.GetMouseButton(0))
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            direction = (mousePosition - transform.position).normalized;
            rb.velocity = new Vector2(direction.x * moveSpeed, 0);
        }
        else {
            rb.velocity = Vector2.zero;
        }
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("enemy"))
        {
            scoreText.GetComponent<Text>().enabled = false;
            GetComponent<AudioSource>().clip = gameOverClip;
            GetComponent<AudioSource>().Play();
            FindObjectOfType<GameManager>().GameOver();
        }
    }
}
