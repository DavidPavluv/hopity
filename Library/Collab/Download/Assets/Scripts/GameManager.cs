﻿using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    //UI
    public Text scoreText;
    private int scoreAmount;

    //GameOverUI
    public GameObject GameOverPanel;
    public Text yourScore;
    
    //bonuses
    public float spawnBonusRate = 30f;
    private float nextBonusSpawn;
    public GameObject lineBonus;
    public GameObject tripleBonus;

    //enemies
    private float nextSpawn;
    public float spawnRate = 3f;
    public GameObject enemy1;
    public GameObject enemy2;
    public GameObject enemy3;
    public GameObject enemy4;
    public GameObject enemy5;

    //spawn area
    private float leftLimitation, rightLimitation;
    Vector2 enemySpawnPos;
    Vector2 bonusSpawnPos;


    //timersObj
    public Image tripleImage;
    public Image lineImage;
    public Text tripleText;
    public Text lineText;

    //DOIT
    public float timeTriple = 0f;
    public float timeLine = 0f;

    public int counter = 0;


    private void Awake()
    {
        nextSpawn = Time.time + 1.5f;
        nextBonusSpawn = Time.time + spawnBonusRate;
    }

    void Start () {
        
        scoreText.text = "0";
        leftLimitation = Camera.main.ViewportToWorldPoint(new Vector2(0, 0)).x + transform.lossyScale.x / 2;
        rightLimitation = Camera.main.ViewportToWorldPoint(new Vector2(1, 0)).x - transform.lossyScale.x / 2;
        nextBonusSpawn = spawnBonusRate;
    }
	
    public void AddScore(int amount)
    {
        scoreAmount += amount;
        scoreText.text = scoreAmount.ToString();
    }

    private void Update()
    {
        //spawning enemies
        if (Time.time > nextSpawn)
        {
            
            float randomX = Random.Range(leftLimitation, rightLimitation);
            enemySpawnPos = new Vector2(randomX, transform.position.y);

            byte randomEnemy = (byte)Random.Range(1, 6);
            switch (randomEnemy)
            {
                case 1:
                    Instantiate(enemy1, enemySpawnPos, Quaternion.identity);
                    break;
                case 2:
                    Instantiate(enemy2, enemySpawnPos, Quaternion.identity);
                    break;
                case 3:
                    Instantiate(enemy3, enemySpawnPos, Quaternion.identity);
                    break;
                case 4:
                    Instantiate(enemy4, enemySpawnPos, Quaternion.identity);
                    break;
                case 5:
                    Instantiate(enemy5, enemySpawnPos, Quaternion.identity);
                    break;
            }

            counter++;
            nextSpawn = Time.time + spawnRate;
        }

        //spawning bonuses
        if(Time.time > nextBonusSpawn)
        {
            byte randomBonus = (byte)Random.Range(1, 7);
            float randomX = Random.Range(leftLimitation, rightLimitation);
            bonusSpawnPos = new Vector2(randomX, transform.position.y);

            if (randomBonus <= 4)
            {
                Instantiate(tripleBonus, bonusSpawnPos, Quaternion.identity);
            } else
            {
                Instantiate(lineBonus, bonusSpawnPos, Quaternion.identity);
            }

            nextBonusSpawn = Time.time + spawnBonusRate;
        }

        if(timeLine > 0.0)
        {
            FindObjectOfType<LineController>().Activate();
            lineImage.GetComponent<Image>().enabled = true;
            lineText.text =  (timeLine -= Time.deltaTime).ToString("F0");


        } else if(timeLine <= 0.0)
        {
            FindObjectOfType<LineController>().Deactivate();
            lineImage.GetComponent<Image>().enabled = false;
        }



        if(timeTriple >  0.0)
        {
            FindObjectOfType<Shooting>().tripleMode = true;
            tripleImage.GetComponent<Image>().enabled = true;
            tripleText.text = (timeTriple -= Time.deltaTime).ToString("F0");
        } else if(timeTriple <= 0.0)
        {
            tripleImage.GetComponent<Image>().enabled = false;
            FindObjectOfType<Shooting>().tripleMode = false;

        }
       
    }

    public void GameOver()
    {
        yourScore.text = scoreText.text;
        scoreText.text = "";
        if (scoreAmount > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", scoreAmount);
        }
        Stop();
        
    }
    void Stop()
    {
        Time.timeScale = 0f;            
        GameOverPanel.SetActive(true);
    }
}
