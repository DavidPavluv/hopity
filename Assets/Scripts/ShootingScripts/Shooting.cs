﻿using UnityEngine;

public class Shooting : MonoBehaviour {
    public GameObject bullet1;
    public GameObject bullet2;
    public GameObject bullet3;
    public GameObject bullet4;
    public GameObject bullet5;
    public GameObject bullet6;
    public GameObject bullet7;
    public GameObject bullet8;
    public GameObject bullet9;
    public GameObject bullet10;
    public GameObject bullet11;
    public GameObject bullet12;
    public GameObject bullet13;
    public GameObject bullet14;
    public GameObject bullet15;
    public GameObject bullet16;
    public GameObject bullet17;
    public GameObject bullet18;
    public GameObject bullet19;
    public GameObject bullet20;
    public GameObject bullet21;
    public GameObject bullet22;





    private GameObject bulletSelected;
    Vector2 pos;
    public float offsetY = 5f;
    float nextFire = 0.0f;
    public float fireRate = 0.5f;
    public bool tripleMode = false;
    public float offsetX = 0.1f;
    public bool rotation = false;

    private void Start()
    {
        switch(PlayerPrefs.GetString("Selected", "1")){

            case "1":
                bulletSelected = bullet1;
                rotation = false;
                break;
            case "2":
                bulletSelected = bullet2;
                rotation = false;
                break;
            case "3":
                bulletSelected = bullet3;
                rotation = true;
                break;
            case "4":
                bulletSelected = bullet4;
                rotation = false;
                break;
            case "5":
                bulletSelected = bullet5;
                rotation = true;
                break;
            case "6":
                bulletSelected = bullet6;
                rotation = true;
                break;
            case "7":
                bulletSelected = bullet7;
                rotation = true;
                break;
            case "8":
                bulletSelected = bullet8;
                rotation = true;
                break;
            case "9":
                bulletSelected = bullet9;
                rotation = true;
                break;
            case "10":
                bulletSelected = bullet10;
                rotation = false;
                break;
            case "11":
                bulletSelected = bullet11;
                rotation = false;
                break;
            case "12":
                bulletSelected = bullet12;
                rotation = false;
                break;
            case "13":
                bulletSelected = bullet13;
                rotation = true;
                break;
            case "14":
                bulletSelected = bullet14;
                rotation = true;
                break;
            case "15":
                bulletSelected = bullet15;
                rotation = true;
                break;
            case "16":
                bulletSelected = bullet16;
                rotation = true;
                break;
            case "17":
                bulletSelected = bullet17;
                rotation = true;
                break;
            case "18":
                bulletSelected = bullet18;
                rotation = false;
                break;
            case "19":
                bulletSelected = bullet19;
                rotation = true;
                break;
            case "20":
                bulletSelected = bullet20;
                rotation = true;
                break;
            case "21":
                bulletSelected = bullet21;
                rotation = true;
                break;
            case "22":
                bulletSelected = bullet22;
                rotation = true;
                break;

}
        }
    void FixedUpdate () {

        if (Time.time > nextFire)   
        {
            pos = transform.position;
            pos += new Vector2(0f, offsetY);

            if (rotation)
            {
                Instantiate(bulletSelected, pos + new Vector2(0f, -0.1f), Quaternion.Euler(new Vector3(0, 0, Random.RandomRange(0f, 360f))));
            }
            else
            {
                Instantiate(bulletSelected, pos + new Vector2(0f, -0.1f), Quaternion.identity);
            }
                

            if (tripleMode)
            {
                if (rotation)
                {
                    Instantiate(bulletSelected, pos + (new Vector2(offsetX, -0.4f)), Quaternion.Euler(new Vector3(0, 0, Random.RandomRange(0f, 360f))));
                    Instantiate(bulletSelected, pos + (new Vector2(-offsetX, -0.4f)), Quaternion.Euler(new Vector3(0, 0, Random.RandomRange(0f, 360f))));

                }
                else
                {
                    Instantiate(bulletSelected, pos + (new Vector2(offsetX, -0.4f)), Quaternion.identity);
                    Instantiate(bulletSelected, pos + (new Vector2(-offsetX, -0.4f)), Quaternion.identity);
                }

                
            }
            
            nextFire = Time.time + fireRate;
        }     
	}
}
